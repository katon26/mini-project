import type { NextPage } from 'next'
import Navbar from '../components/Navbar';
import Message from './content/Message';

const Home: NextPage = () => {
  return (
    <>
    <Navbar/>
    <Message/>
    </>
  )
}

export default Home
