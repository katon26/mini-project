import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton, { IconButtonProps } from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import ListItemText from '@mui/material/ListItemText';
import ListItemIcon from '@mui/material/ListItemIcon';
import { red } from '@mui/material/colors';
import CommentIcon from '@mui/icons-material/Comment';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import DateRangeIcon from '@mui/icons-material/DateRange';
import Divider from '@mui/material/Divider';

import Box from '@mui/material/Box';
import TextField, { TextFieldProps } from '@mui/material/TextField';
import Stack from '@mui/material/Stack';

import SendIcon from '@mui/icons-material/Send';

interface ExpandMoreProps extends IconButtonProps {
  expand: boolean;
}

const CssTextField = styled(TextField)({
  '& label.Mui-focused': {
    color: 'grey',
  },
  '& .MuiInput-underline:after': {
    borderBottomColor: 'red',
  },
  '& .MuiOutlinedInput-root': {
    '&:hover fieldset': {
      borderColor: 'red',
    },
    '&.Mui-focused fieldset': {
      borderColor: 'red',
    },
  },
});

const ExpandMore = styled((props: ExpandMoreProps) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

export default function RecipeReviewCard() {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card sx={{ 
        // maxWidth: 700, 
        direction:'column',
        justifyContent:'center',
        alignItems:'stretch',
        spacing:2,
        mt:2,
    }}
    
    >
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
            K
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title="Katon Adam"
        subheader={
          "22 February 2020"
          // <ListItemIcon>
          // </ListItemIcon>
        }
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
        Let’s check to see if it works. 
        You should now have a link on each page, allowing you to go back and forth
        </Typography>
      </CardContent>

      <CardActions disableSpacing>
        <ExpandMore 
          aria-label="comment"
          expand={false}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          
        >
          <CommentIcon sx={{ mr: 0.5 }} />
          <Typography 
            variant="body2" 
            color="text.secondary"
          >
            1 Comment
          </Typography>
        </ExpandMore>
        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
      <Divider/>        
          <CardHeader
          avatar={
            <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
              K
            </Avatar>
          }
          title="Katon Adam"
          subheader={
            "22 February 2020"
            // <ListItemIcon>
            // </ListItemIcon>
          }
        />
        
        <CardContent>
          <CardContent>
            <Typography variant="body2" color="text.secondary">
            Example of a comment. the height of the text field dynamically matches its content
          </Typography>
          </CardContent>

          <Stack>

          </Stack>

          <Box sx={{}}>
            <CssTextField
              fullWidth sx={{ mt: 5, borderColor: 'red' }}
              id="outlined-textarea"
              label="Write your comment"
              placeholder="..."
              multiline
            />
          </Box>
          
          
          {/* <Fab color="primary" aria-label="add">
            <SendIcon>
              asd
            </SendIcon>
          </Fab> */}

        </CardContent>

      </Collapse>
    </Card>
  );
}
