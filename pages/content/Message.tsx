import * as React from 'react';
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import { styled } from '@mui/material/styles';
import CardList from './CardList';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'left',
  color: theme.palette.text.primary,
}));

export default function ResponsiveStack() {
  return (
    <div>
        <Container fixed>
            {/* <Box sx={{ bgcolor: '#cfe8fc', height: '100vh' }} /> */}
            <Stack
                // direction={{ xs: 'column', sm: 'row' }}
                // spacing={{ xs: 1, sm: 2, md: 4 }}
                direction="column"
                justifyContent="center"
                alignItems="stretch"
                spacing={3}
                mt={3}
                mb={3}
            >
                <CardList/>
                <CardList/>
                <CardList/>
                <CardList/>
                <CardList/>


                
                {/* <Item>Item 2</Item>
                <Item>Item 3</Item> */}
            </Stack>
        </Container>
      
    </div>
  );
}
